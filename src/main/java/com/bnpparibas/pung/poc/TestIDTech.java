package com.bnpparibas.pung.poc;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.query.FieldsQueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.DataRegionConfiguration;
import org.apache.ignite.configuration.DataStorageConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;
import java.util.concurrent.locks.Lock;

public class TestIDTech {


    public static void main (String[] args) throws ClassNotFoundException, SQLException, InterruptedException {


        Ignite ignite = Ignition.start();

        AtomicLong sequence = new AtomicLong();

        IgniteCache<String,String> lockCache = ignite.getOrCreateCache(
                new CacheConfiguration<String,String>()
                        .setName("lockCache")
                        .setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL)
        );


        IgniteCache<String,Long> idConvCache = ignite.getOrCreateCache(
                new CacheConfiguration<String,Long>().setName("idConv")
        );



        IgniteCache<Long,Position> positionCache = ignite.getOrCreateCache(
                new CacheConfiguration<Long,Position>().setName("pos")
        );


        IgniteCache<Long,Dossier> dossierCache = ignite.getOrCreateCache(
                new CacheConfiguration<Long,Dossier>().setName("dos")
        );

        //int numberOfPosPerDossier = 1000;
        //int numberOfDossier = 5;

        int numberOfPosPerDossier = 65_000;
        int numberOfDossier = 450;

        int numberOfPos = numberOfPosPerDossier * numberOfDossier;

        ThreadPoolExecutor threadPoolExecutorPosition = new ThreadPoolExecutor(4, 4, 10, TimeUnit.SECONDS, new LinkedBlockingDeque<>());
        ThreadPoolExecutor threadPoolExecutorDossier = new ThreadPoolExecutor(4, 4, 10, TimeUnit.SECONDS, new LinkedBlockingDeque<>());

        long begin = System.currentTimeMillis();

        List<Callable<Integer>> listPositionTask = new ArrayList<>(200);
        List<Callable<Integer>> listDossierTask = new ArrayList<>(200);

        for (int dossierIndexPage = 0; dossierIndexPage < numberOfDossier / 10; dossierIndexPage++) {
            //dossierCache.put((long)dossierIndex,new Dossier((long) dossierIndex,""+dossierIndex));
            int finalDossierIndexPage = dossierIndexPage;
            listDossierTask.add(() -> {
                for ( int index = 0 ; index < 10 ; index ++ ) {
                    int dossierIndex = finalDossierIndexPage * 10 + index;
                    String funcKey = "dossier_" + dossierIndex;
                    Long id = getOrGenerateId(sequence, lockCache, idConvCache, funcKey,"dos");
                    dossierCache.put(id,new Dossier(id,"orig"+dossierIndex));
                    //System.out.println("DOS "+dossierIndex+" -> "+id);
                }
                return 0;
            });
        }

        for (int positionIndexpage = 0; positionIndexpage < numberOfPos / 10; positionIndexpage++) {
            //dossierCache.put((long)dossierIndex,new Dossier((long) dossierIndex,""+dossierIndex));
            int finalPositionIndexpage = positionIndexpage;
            listPositionTask.add(() -> {
                for ( int index = 0 ; index < 10 ; index ++ ) {
                    int positionIndex = finalPositionIndexpage * 10 + index;
                    int dossierIndex = positionIndex % numberOfDossier;
                    String funcKey = "dossier_" + dossierIndex;
                    Long id = getOrGenerateId(sequence, lockCache, idConvCache, funcKey,"pos");
                    positionCache.put((long)positionIndex,new Position((long)positionIndex,"orig"+dossierIndex,id));
                }
                return 0;
            });
        }

        Thread watchDog = new Thread(
            () -> {
                long previousTime = System.currentTimeMillis();
                long previousCount = 0;

                while (positionCache.size() < numberOfPos) {
                    long currentTime = System.currentTimeMillis();
                    long currentCount = positionCache.size();
                    long currentDiff = currentCount - previousCount;
                    long delay = currentTime - previousTime;
                    previousTime = currentTime;
                    previousCount = currentCount;
                    System.out.println("" + dossierCache.size() + "\t"+ currentCount + "\t" + currentTime + "\t" + currentDiff + "\t" + delay + "\t" + (delay == 0 ? 0 : currentDiff / delay));
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        );

        watchDog.start();
        listDossierTask.forEach(threadPoolExecutorDossier::submit);
        listPositionTask.forEach(threadPoolExecutorPosition::submit);
        watchDog.join();

        long end = System.currentTimeMillis();

        long total = end - begin;

        System.out.println(" total = " + total);


    }

    @NotNull
    private static Long getOrGenerateId(AtomicLong sequence, IgniteCache<String, String> lockCache, IgniteCache<String, Long> idConvCache, String funcKey,String genfrom) {
        Long id = idConvCache.get(funcKey);
        if ( id==null ) {
            Lock lock = lockCache.lock(funcKey);
            try {
                lock.lock();
                id = idConvCache.get(funcKey);
                if ( id ==null ) {
                    id = sequence.getAndIncrement();
                    idConvCache.put(funcKey,id);
                    System.out.println("genfrom "+genfrom+" "+funcKey+" -> "+id);
                }
            } finally {
                lock.unlock();
            }
        }
        return id;
    }

    private static void execQuery(Connection conn, String sql) {
        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
