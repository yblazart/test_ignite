package com.bnpparibas.pung.poc;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.cache.query.FieldsQueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.DataRegionConfiguration;
import org.apache.ignite.configuration.DataStorageConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.spi.IgniteSpiException;
import org.apache.ignite.spi.discovery.DiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.TcpDiscoveryIpFinder;
import org.apache.ignite.spi.discovery.tcp.ipfinder.TcpDiscoveryIpFinderAdapter;
import org.apache.ignite.spi.discovery.tcp.ipfinder.vm.TcpDiscoveryVmIpFinder;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class TestQualif {


    public static final String IP = "192.168.2.177";
    public static final int NUMBER_OF_POS_PER_DOSSIER = 65_000;
    public static final int NUMBER_OF_DOSSIER = 450;

    public static void main(String[] args) throws ClassNotFoundException, SQLException, InterruptedException {
        Ignition.setClientMode(true);

        IgniteConfiguration cfg = new IgniteConfiguration();
        TcpDiscoverySpi discoSpi = new TcpDiscoverySpi();
        TcpDiscoveryIpFinder ipFinder = new TcpDiscoveryVmIpFinder();
        ((TcpDiscoveryVmIpFinder) ipFinder).setAddresses(Arrays.asList(IP));

        discoSpi.setIpFinder(ipFinder);
        cfg.setDiscoverySpi(discoSpi);


        Ignite ignite = Ignition.start(cfg);

        IgniteCache<Long, Position> positionCache = ignite.getOrCreateCache(
                new CacheConfiguration<Long, Position>()
                        .setName("PositionCache")
                        .setSqlSchema("SCH001")  // needed to have tables in same schema, then be able to do select join
                        .setCacheMode(CacheMode.PARTITIONED)
                        .setIndexedTypes(Long.class, Position.class));


        IgniteCache<Long, Dossier> dossierCache = ignite.getOrCreateCache(
                new CacheConfiguration<Long, Dossier>()
                        .setName("DossierCache")
                        .setSqlSchema("SCH001")
                        .setCacheMode(CacheMode.REPLICATED)
                        .setIndexedTypes(Long.class, Dossier.class));


        //int numberOfPosPerDossier = 1000;
        //int numberOfDossier = 5;


        int numberOfPos = NUMBER_OF_POS_PER_DOSSIER * NUMBER_OF_DOSSIER;

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(8, 8, 10, TimeUnit.SECONDS, new LinkedBlockingDeque<>());

        long begin = System.currentTimeMillis();

        List<Callable<Integer>> list = new ArrayList<>(200);

        for (int dossierIndex = 0; dossierIndex < NUMBER_OF_DOSSIER; dossierIndex++) {
            System.out.println("prepare dossierIndex = " + dossierIndex);
            int finalDossierIndex = dossierIndex;
            list.add(() -> {
                dossierCache.put((long) finalDossierIndex, new Dossier((long) finalDossierIndex, "" + finalDossierIndex));

                Map<Long, Position> preload = new TreeMap<>();
                for (int posIndex = 0; posIndex < NUMBER_OF_POS_PER_DOSSIER; posIndex++) {
                    int realIndex = (finalDossierIndex * NUMBER_OF_POS_PER_DOSSIER) + posIndex;
                    preload.put((long) realIndex, new Position((long) realIndex, "" + realIndex + "_" + finalDossierIndex, (long) finalDossierIndex));

                    if (preload.size() == 10_000) {
                        positionCache.putAll(preload);
                        preload.clear();
                    }

                }
                if (!preload.isEmpty()) {
                    positionCache.putAll(preload);
                    preload.clear();
                }
                return 0;
            });
        }

        Thread watchDog = new Thread(
                () -> {
                    long previousTime = System.currentTimeMillis();
                    long previousCount = 0;

                    while (positionCache.size() < numberOfPos) {
                        long currentTime = System.currentTimeMillis();
                        long currentCount = positionCache.size();
                        long currentDiff = currentCount - previousCount;
                        long delay = currentTime - previousTime;
                        previousTime = currentTime;
                        previousCount = currentCount;
                        System.out.println("" + currentCount + "\t" + currentTime + "\t" + currentDiff + "\t" + delay + "\t" + (delay == 0 ? 0 : currentDiff / delay));
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );

        watchDog.start();
        threadPoolExecutor.invokeAll(list);
        watchDog.join();

        long end = System.currentTimeMillis();

        long total = end - begin;

        System.out.println(" total = " + total);


        begin = System.currentTimeMillis();
        FieldsQueryCursor query = dossierCache.query(new SqlFieldsQuery("SELECT DISTINCT d.id from Dossier d where exists (select * from Position p where p.dossierId = d.id )"));


        long count = query.getAll().stream().count();

        end = System.currentTimeMillis();

        total = end - begin;

        System.out.println(" req = " + total + "ms for " + count+" elet");
        ignite.close();
        System.exit(0);
    }

    private static void execQuery(Connection conn, String sql) {
        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
