package com.bnpparibas.pung.poc;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

import java.io.Serializable;


//  Dossier ( id LONG PRIMARY KEY, code VARCHAR )
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Dossier implements Serializable {


    @QuerySqlField(index = true)
    private Long id;

    @QuerySqlField
    private String code;

}
