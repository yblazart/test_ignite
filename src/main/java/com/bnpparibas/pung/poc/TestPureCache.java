package com.bnpparibas.pung.poc;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class TestPureCache {


    public static void main (String[] args) throws ClassNotFoundException, SQLException, InterruptedException {
        Ignite ignite = Ignition.start();

        IgniteCache<Integer, Integer> cache = ignite.getOrCreateCache("testU");


        int numberOfPosPerDossier = 65_000;
        int numberOfDossier = 450;
        int numberOfPos = numberOfPosPerDossier * numberOfDossier;

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(8, 8, 10, TimeUnit.SECONDS, new LinkedBlockingDeque<>());

        long begin = System.currentTimeMillis();

        List<Callable<Integer>> list = new ArrayList<>(200);

        for (int dossierIndex = 0; dossierIndex < numberOfDossier; dossierIndex++) {
            System.out.println("prepare dossierIndex = " + dossierIndex);

            int finalDossierIndex = dossierIndex;
            list.add(() -> {

                    for (int posIndex = 0; posIndex < numberOfPosPerDossier; posIndex++) {
                        int realIndex = (finalDossierIndex * numberOfPosPerDossier) + posIndex;
                        cache.put(realIndex,realIndex);
                    }
                return 0;
            });
        }

        Thread watchDog = new Thread(
            () -> {
                long previousTime = System.currentTimeMillis();
                long previousCount = 0;

                while (cache.size() < numberOfPos) {
                    long currentTime = System.currentTimeMillis();
                    long currentCount = cache.size();
                    long currentDiff = currentCount - previousCount;
                    long delay = currentTime - previousTime;
                    previousTime = currentTime;
                    previousCount = currentCount;
                    System.out.println("" + currentCount + "\t" + currentTime + "\t" + currentDiff + "\t" + delay + "\t" + (delay == 0 ? 0 : currentDiff / delay));
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        );

        watchDog.start();
        threadPoolExecutor.invokeAll(list);
        watchDog.join();

        long end = System.currentTimeMillis();

        long total = end - begin;

        System.out.println(" total = " + total);

        //ignite.close();
    }

}
