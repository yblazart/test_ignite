package com.bnpparibas.pung.poc;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.query.FieldsQueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.DataRegionConfiguration;
import org.apache.ignite.configuration.DataStorageConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class TestPureCacheAndSQL {


    public static void main (String[] args) throws ClassNotFoundException, SQLException, InterruptedException {
        IgniteConfiguration cfg=new IgniteConfiguration();
        DataStorageConfiguration dataStorageConfiguration=new DataStorageConfiguration();

        DataRegionConfiguration dataRegionConfiguration=new DataRegionConfiguration();
        dataRegionConfiguration.setName("TestRegion");
        dataRegionConfiguration.setMaxSize(1024 *1024*1024*20L);
        dataRegionConfiguration.setInitialSize(1024 *1024*1024L);


        dataStorageConfiguration.setDataRegionConfigurations(dataRegionConfiguration);
        cfg.setDataStorageConfiguration(dataStorageConfiguration);


        Ignite ignite = Ignition.start(cfg);

        IgniteCache<Long,Position> positionCache = ignite.getOrCreateCache(
                new CacheConfiguration<Long,Position>()
                        .setName("PositionCache")
                        .setSqlSchema("SCH001")  // needed to have tables in same schema, then be able to do select join
                        .setDataRegionName("TestRegion")
                        .setIndexedTypes(Long.class, Position.class));


        IgniteCache<Long,Dossier> dossierCache = ignite.getOrCreateCache(
                new CacheConfiguration<Long,Dossier>()
                        .setName("DossierCache")
                        .setSqlSchema("SCH001")
                        .setDataRegionName("TestRegion")
                        .setIndexedTypes(Long.class, Dossier.class));




        //int numberOfPosPerDossier = 1000;
        //int numberOfDossier = 5;

        int numberOfPosPerDossier = 65_000;
        int numberOfDossier = 450;


        int numberOfPos = numberOfPosPerDossier * numberOfDossier;

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(8, 8, 10, TimeUnit.SECONDS, new LinkedBlockingDeque<>());

        long begin = System.currentTimeMillis();

        List<Callable<Integer>> list = new ArrayList<>(200);

        for (int dossierIndex = 0; dossierIndex < numberOfDossier; dossierIndex++) {
            System.out.println("prepare dossierIndex = " + dossierIndex);
            int finalDossierIndex = dossierIndex;
            list.add(() -> {
            dossierCache.put((long)finalDossierIndex,new Dossier((long) finalDossierIndex,""+finalDossierIndex));

                    for (int posIndex = 0; posIndex < numberOfPosPerDossier; posIndex++) {
                        int realIndex = (finalDossierIndex * numberOfPosPerDossier) + posIndex;
                        positionCache.put((long)realIndex,new Position((long)realIndex,""+realIndex+"_"+finalDossierIndex, (long) finalDossierIndex));
                    }
                return 0;
            });
        }

        Thread watchDog = new Thread(
            () -> {
                long previousTime = System.currentTimeMillis();
                long previousCount = 0;

                while (positionCache.size() < numberOfPos) {
                    long currentTime = System.currentTimeMillis();
                    long currentCount = positionCache.size();
                    long currentDiff = currentCount - previousCount;
                    long delay = currentTime - previousTime;
                    previousTime = currentTime;
                    previousCount = currentCount;
                    System.out.println("" + currentCount + "\t" + currentTime + "\t" + currentDiff + "\t" + delay + "\t" + (delay == 0 ? 0 : currentDiff / delay));
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        );

        watchDog.start();
        threadPoolExecutor.invokeAll(list);
        watchDog.join();

        long end = System.currentTimeMillis();

        long total = end - begin;

        System.out.println(" total = " + total);


        begin = System.currentTimeMillis();
        FieldsQueryCursor query = dossierCache.query(new SqlFieldsQuery("SELECT DISTINCT d.id from Dossier d where exists (select * from Position p where p.dossierId = d.id )"));


        long count = query.getAll().stream().count();

        end = System.currentTimeMillis();

        total = end - begin;

        System.out.println(" req = " + total+" for "+count);
        //ignite.close();
    }
    private static void execQuery(Connection conn, String sql) {
        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
