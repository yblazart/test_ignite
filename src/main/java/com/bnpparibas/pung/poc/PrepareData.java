package com.bnpparibas.pung.poc;

import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.LongAdder;

public class PrepareData {

    private static String createAllSQL[] = {
            " DROP INDEX IF EXISTS IDX_POSDOSSIERID ; ",
            " DROP TABLE IF EXISTS Dossier ; ",
            " DROP TABLE IF EXISTS Position ; ",
            " CREATE TABLE IF NOT EXISTS Position ( id LONG PRIMARY KEY, code VARCHAR, dossierId LONG ) WITH \"template=partitioned\" ; ",
            " CREATE TABLE IF NOT EXISTS Dossier ( id LONG PRIMARY KEY, code VARCHAR ) WITH \"template=replicated\" ; ",
            " CREATE INDEX IF NOT EXISTS IDX_POSDOSSIERID on Position ( dossierId ASC ) PARALLEL 8 ; "
    };

    public static void main (String[] args) throws ClassNotFoundException, SQLException, InterruptedException {
        Ignite ignite = Ignition.start();

        Class.forName("org.apache.ignite.IgniteJdbcDriver");

        try (Connection conn = DriverManager.getConnection("jdbc:ignite:thin://127.0.0.1/")) {
            conn.setAutoCommit(true);
            Arrays.stream(createAllSQL).forEach(sql -> {
                exec(conn, sql);
            });
        }

        int numberOfPosPerDossier = 100_000;
        int numberOfDossier = 600;
        int numberOfPos = numberOfPosPerDossier * numberOfDossier;

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(8, 8, 10, TimeUnit.SECONDS, new LinkedBlockingDeque<>());

        long begin = System.currentTimeMillis();

        List<Callable<Integer>> list = new ArrayList<>(200);

        for (int dossierIndex = 0; dossierIndex < numberOfDossier; dossierIndex++) {
            System.out.println("prepare dossierIndex = " + dossierIndex);

            int finalDossierIndex = dossierIndex;
            list.add(() -> {
                try (Connection conn = DriverManager.getConnection("jdbc:ignite:thin://127.0.0.1/")) {
                    conn.setAutoCommit(true);
                    exec(conn, "INSERT INTO Dossier(id, code) values(?,?);", finalDossierIndex, "code" + finalDossierIndex);

                    for (int posIndex = 0; posIndex < numberOfPosPerDossier; posIndex++) {
                        int realIndex = (finalDossierIndex * numberOfPosPerDossier) + posIndex;
                        exec(conn, "INSERT INTO Position(id, code, dossierId) values(?,?,?);", realIndex, "code" + realIndex, finalDossierIndex);
                    }
                    conn.commit();
                }
                return 0;
            });
        }

        Thread watchDog = new Thread(
            () -> {
                long previousTime = System.currentTimeMillis();
                long previousCount = 0;
                AtomicInteger totoSize = new AtomicInteger();
                countPos(totoSize);

                while (totoSize.get() < numberOfPos) {
                    long currentTime = System.currentTimeMillis();
                    long currentCount = totoSize.get();
                    long currentDiff = currentCount - previousCount;
                    long delay = currentTime - previousTime;
                    previousTime = currentTime;
                    previousCount = currentCount;
                    System.out.println("" + currentCount + "\t" + currentTime + "\t" + currentDiff + "\t" + delay + "\t" + (delay == 0 ? 0 : currentDiff / delay));
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    countPos(totoSize);
                }
            }
        );

        watchDog.start();
        threadPoolExecutor.invokeAll(list);
        watchDog.join();

        long end = System.currentTimeMillis();

        long total = end - begin;

        System.out.println(" total = " + total);

        try (Connection conn = DriverManager.getConnection("jdbc:ignite:thin://127.0.0.1/")) {
            execQuery(conn, "SELECT DISTINCT d.id from Dossier d where exists (select * from Position p where p.dossierId = d.id ) ");
        }

        ignite.close();

        end = System.currentTimeMillis();

        total = end - begin;

        System.out.println(" total = " + total);
    }

    private static void countPos(AtomicInteger totoSize) {
        try (Connection conn = DriverManager.getConnection("jdbc:ignite:thin://127.0.0.1/")) {
            try (PreparedStatement ps = conn.prepareStatement("select count(*) from Position")) {
                try (ResultSet resultSet = ps.executeQuery()) {
                    resultSet.next();
                    totoSize.set(resultSet.getInt(1));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void execQuery(Connection conn, String sql) {
        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void exec(Connection conn, String sql) {
        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void exec(Connection conn, String sql, Object... params) {
        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            for (int pos = 0; pos < params.length; pos++) {
                ps.setObject(pos + 1, params[pos]);
            }
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
