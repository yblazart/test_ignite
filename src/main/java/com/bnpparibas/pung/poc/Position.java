package com.bnpparibas.pung.poc;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

import java.io.Serializable;


// CREATE TABLE IF NOT EXISTS Position ( id LONG PRIMARY KEY, code VARCHAR, dossierId LONG )
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Position implements Serializable {


    @QuerySqlField(index = true)
    private Long id;

    @QuerySqlField
    private String code;

    @QuerySqlField(index = true)
    private Long dossierId;

}
